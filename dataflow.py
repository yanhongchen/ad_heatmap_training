from keras.utils.io_utils import HDF5Matrix
from skimage.io import ImageCollection, imread
from itertools import chain
import os
import random
from glob import glob
import numpy as np
import csv
from keras.utils import Sequence

class ImageFlow_from_directory(Sequence):
    def __init__(self, path, batch_size=32, seed=None, shuffle=True, datagen=None, resize=None, addition_datasource=None):
        if resize:
            print('Resize input to ({}, {}).'.format(resize[0], resize[1]))

        self.datagen = datagen
        self.resize = resize
        self.heatmap_size = (32, 32)
        self.batch_size = batch_size
        self.shuffle = shuffle

        if seed: random.seed(seed)

        self.path_list = []
        self.mask_paths = []

        f = open(path, 'r')
        for row in csv.DictReader(f):
            mask_path = row['mask_path']
            img_path = row['img_path']

            self.path_list.append(img_path)
            self.mask_paths.append(mask_path)
        f.close()

        self.mask_paths = np.array(self.mask_paths)
        self.path_list = np.array(self.path_list)
        self.indices = [i for i in range(len(self.mask_paths))]
        if shuffle:
            random.shuffle(self.indices)

    def __len__(self):
        from numpy import ceil
        return int(ceil(len(self.mask_paths) / self.batch_size))

    def __getitem__(self, idx):
        import numpy as np
        from cv2 import resize, imread, IMREAD_GRAYSCALE
        import skimage.measure

        indices = self.indices[idx * self.batch_size:(idx + 1) * self.batch_size]
        indices = sorted(indices)
        mask_paths = self.mask_paths[indices]

        batch_y = []
        for path in mask_paths:
            mask = imread(path, IMREAD_GRAYSCALE)
            mask = skimage.measure.block_reduce(mask, (74, 74), np.max)
            mask = (mask / 127.5) - 1
            batch_y.append(mask)

        batch_y = np.expand_dims(batch_y, -1)
        
        if self.resize:
            batch_x = np.array([resize(imread(path, -1), self.resize) for path in self.path_list[indices]])
        else:
            batch_x = np.array([imread(path, -1) for path in self.path_list[indices]])
        batch_x = np.expand_dims(batch_x, -1)

        return batch_x, batch_y

    def on_epoch_end(self):
        import random
        if self.shuffle:
            random.shuffle(self.indices)
    '''
    def class_balance_weight(self):
        import numpy as np
        y = self.y[:]
        return {1:(len(y)-np.sum(y))/len(y), 0:np.sum(y)/len(y)}
    '''
